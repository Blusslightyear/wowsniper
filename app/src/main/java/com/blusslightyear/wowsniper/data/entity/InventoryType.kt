package com.blusslightyear.wowsniper.data.entity

data class InventoryType(
    val name: String,
    val type: String
)