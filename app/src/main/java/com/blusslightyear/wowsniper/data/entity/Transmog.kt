package com.blusslightyear.wowsniper.data.entity

data class Transmog(
    val display_string: String,
    val item: Item,
    val item_modified_appearance_id: Int
)