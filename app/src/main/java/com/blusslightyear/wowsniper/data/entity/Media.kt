package com.blusslightyear.wowsniper.data.entity

data class Media(
    val id: Int,
    val key: Key
)