package com.blusslightyear.wowsniper.data.entity

data class Spell(
    val id: Int,
    val key: Key,
    val name: String
)