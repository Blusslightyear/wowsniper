package com.blusslightyear.wowsniper.data.entity

data class Character(
    val id: Int,
    val key: Key,
    val name: String,
    val realm: Realm
)