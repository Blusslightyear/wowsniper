package com.blusslightyear.wowsniper.data.entity

data class Slot(
    val name: String,
    val type: String
)