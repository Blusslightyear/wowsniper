package com.blusslightyear.wowsniper.data.entity

data class SocketType(
    val name: String,
    val type: String
)