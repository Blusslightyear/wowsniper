package com.blusslightyear.wowsniper.data.entity

data class Item(
    val id: Int,
    val key: Key,
    val name: String
)