package com.blusslightyear.wowsniper.data.entity

data class EnchantmentSlot(
    val id: Int,
    val type: String
)