package com.blusslightyear.wowsniper.data.entity

data class Realm(
    val id: Int,
    val key: Key,
    val name: String,
    val slug: String
)