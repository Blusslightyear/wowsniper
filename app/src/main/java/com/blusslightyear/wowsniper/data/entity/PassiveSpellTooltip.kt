package com.blusslightyear.wowsniper.data.entity

data class PassiveSpellTooltip(
    val cast_time: String,
    val description: String,
    val range: String,
    val spell: Spell
)