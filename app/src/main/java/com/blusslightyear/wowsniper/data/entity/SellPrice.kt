package com.blusslightyear.wowsniper.data.entity

data class SellPrice(
    val display_strings: DisplayStrings,
    val value: Int
)