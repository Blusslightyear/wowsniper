package com.blusslightyear.wowsniper.data.entity

data class Type(
    val name: String,
    val type: String
)