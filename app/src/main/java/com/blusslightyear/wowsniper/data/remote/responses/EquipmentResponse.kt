package com.blusslightyear.wowsniper.data.remote.responses

import com.blusslightyear.wowsniper.data.entity.Character
import com.blusslightyear.wowsniper.data.entity.EquippedItem
import com.blusslightyear.wowsniper.data.entity.Links

data class EquipmentResponse(
    val _links: Links,
    val character: Character,
    val equipped_items: List<EquippedItem>
)