package com.blusslightyear.wowsniper.data.entity

data class SourceItem(
    val id: Int,
    val key: Key,
    val name: String
)