package com.blusslightyear.wowsniper.data.entity

data class Binding(
    val name: String,
    val type: String
)