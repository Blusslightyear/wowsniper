package com.blusslightyear.wowsniper.data.entity

data class SpellTooltip(
    val cast_time: String,
    val description: String,
    val spell: Spell
)