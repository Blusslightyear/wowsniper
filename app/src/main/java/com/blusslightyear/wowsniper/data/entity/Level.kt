package com.blusslightyear.wowsniper.data.entity

data class Level(
    val display_string: String,
    val value: Int
)