package com.blusslightyear.wowsniper.data.entity

data class Display(
    val color: Color,
    val display_string: String
)