package com.blusslightyear.wowsniper.data.entity

data class NameDescription(
    val color: Color,
    val display_string: String
)