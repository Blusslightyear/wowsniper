package com.blusslightyear.wowsniper.data.entity

data class Skill(
    val display_string: String,
    val level: Int,
    val profession: Profession
)