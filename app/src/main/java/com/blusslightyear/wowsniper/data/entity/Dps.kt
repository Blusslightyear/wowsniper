package com.blusslightyear.wowsniper.data.entity

data class Dps(
    val display_string: String,
    val value: Double
)