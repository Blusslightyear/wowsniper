package com.blusslightyear.wowsniper.data.remote.responses

data class TokenResponse(
    val access_token: String,
    val expires_in: Int,
    val token_type: String
)