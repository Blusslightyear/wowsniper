package com.blusslightyear.wowsniper.data.entity

data class Enchantment(
    val display_string: String,
    val enchantment_id: Int,
    val enchantment_slot: EnchantmentSlot,
    val source_item: SourceItem,
    val spell: Spell
)