package com.blusslightyear.wowsniper.data.entity

data class ItemSubclass(
    val id: Int,
    val key: Key,
    val name: String
)