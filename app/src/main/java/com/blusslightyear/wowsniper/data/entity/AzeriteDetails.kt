package com.blusslightyear.wowsniper.data.entity

data class AzeriteDetails(
    val level: Level,
    val percentage_to_next_level: Double,
    val selected_essences: List<SelectedEssence>,
    val selected_powers: List<SelectedPower>,
    val selected_powers_string: String
)