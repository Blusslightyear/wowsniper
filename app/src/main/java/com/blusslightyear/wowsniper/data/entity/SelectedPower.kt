package com.blusslightyear.wowsniper.data.entity

data class SelectedPower(
    val id: Int,
    val is_display_hidden: Boolean,
    val spell_tooltip: SpellTooltip,
    val tier: Int
)