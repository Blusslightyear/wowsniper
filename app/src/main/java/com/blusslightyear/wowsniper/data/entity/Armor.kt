package com.blusslightyear.wowsniper.data.entity

data class Armor(
    val display: Display,
    val value: Int
)