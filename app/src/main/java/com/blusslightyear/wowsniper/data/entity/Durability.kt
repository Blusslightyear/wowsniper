package com.blusslightyear.wowsniper.data.entity

data class Durability(
    val display_string: String,
    val value: Int
)