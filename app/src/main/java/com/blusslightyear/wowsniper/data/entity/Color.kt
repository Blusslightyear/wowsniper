package com.blusslightyear.wowsniper.data.entity

data class Color(
    val a: Double,
    val b: Int,
    val g: Int,
    val r: Int
)