package com.blusslightyear.wowsniper.data.remote.api

import com.blusslightyear.wowsniper.BuildConfig
import com.blusslightyear.wowsniper.data.remote.responses.TokenResponse
import com.blusslightyear.wowsniper.utils.Constants.CLIENT_CREDENTIALS
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface BlizzardAPI {

    @GET("oauth/token")
    suspend fun getAccessToken(
        @Query("client_id") clientIdKey: String = BuildConfig.CLIENT_ID_KEY,
        @Query("client_secret") clientSecretKey: String = BuildConfig.CLIENT_SECRET_KEY,
        @Query("grant_type") grant_type: String = CLIENT_CREDENTIALS
    ): Response<TokenResponse>
}