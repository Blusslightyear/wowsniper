package com.blusslightyear.wowsniper.data.entity

data class AttackSpeed(
    val display_string: String,
    val value: Int
)