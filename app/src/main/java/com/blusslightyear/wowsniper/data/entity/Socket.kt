package com.blusslightyear.wowsniper.data.entity

data class Socket(
    val display_string: String,
    val item: Item,
    val media: Media,
    val socket_type: SocketType
)