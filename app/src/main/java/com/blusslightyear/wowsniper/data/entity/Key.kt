package com.blusslightyear.wowsniper.data.entity

data class Key(
    val href: String
)