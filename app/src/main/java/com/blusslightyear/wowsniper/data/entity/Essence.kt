package com.blusslightyear.wowsniper.data.entity

data class Essence(
    val id: Int,
    val key: Key,
    val name: String
)