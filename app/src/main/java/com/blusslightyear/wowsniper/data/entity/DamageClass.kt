package com.blusslightyear.wowsniper.data.entity

data class DamageClass(
    val name: String,
    val type: String
)