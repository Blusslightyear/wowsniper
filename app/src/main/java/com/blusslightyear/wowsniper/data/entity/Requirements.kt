package com.blusslightyear.wowsniper.data.entity

data class Requirements(
    val level: Level,
    val skill: Skill
)