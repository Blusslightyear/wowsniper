package com.blusslightyear.wowsniper.data.entity

data class SelectedEssence(
    val essence: Essence,
    val main_spell_tooltip: MainSpellTooltip,
    val media: Media,
    val passive_spell_tooltip: PassiveSpellTooltip,
    val rank: Int,
    val slot: Int
)