package com.blusslightyear.wowsniper.data.entity

data class MainSpellTooltip(
    val cast_time: String,
    val cooldown: String,
    val description: String,
    val range: String,
    val spell: Spell
)