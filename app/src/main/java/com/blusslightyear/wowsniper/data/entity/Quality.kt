package com.blusslightyear.wowsniper.data.entity

data class Quality(
    val name: String,
    val type: String
)