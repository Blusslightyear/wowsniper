package com.blusslightyear.wowsniper.data.entity

data class Weapon(
    val attack_speed: AttackSpeed,
    val damage: Damage,
    val dps: Dps
)