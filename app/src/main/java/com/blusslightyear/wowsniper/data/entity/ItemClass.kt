package com.blusslightyear.wowsniper.data.entity

data class ItemClass(
    val id: Int,
    val key: Key,
    val name: String
)