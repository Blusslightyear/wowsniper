package com.blusslightyear.wowsniper

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class WoWSniperApplication : Application()