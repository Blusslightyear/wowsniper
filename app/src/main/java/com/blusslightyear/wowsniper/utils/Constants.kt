package com.blusslightyear.wowsniper.utils

object Constants {

    // API
    const val BASE_URL = "https://eu.battle.net/"
    const val CLIENT_CREDENTIALS = "client_credentials"

}