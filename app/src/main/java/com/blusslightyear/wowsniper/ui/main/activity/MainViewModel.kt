package com.blusslightyear.wowsniper.ui.main.activity

import android.util.Log
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import com.blusslightyear.myreminder.ui.base.viewmodel.BaseViewModel
import com.blusslightyear.wowsniper.data.remote.api.BlizzardAPI
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class MainViewModel @ViewModelInject constructor(
    val blizzardApi: BlizzardAPI,
    @Assisted private val savedStateHandle: SavedStateHandle
) : BaseViewModel() {

    fun getToken() {
        GlobalScope.launch(Dispatchers.IO) {
            val response = blizzardApi.getAccessToken()
            if (response.isSuccessful) {
                Log.d("GetBlizzardToken", response.body()!!.toString())
            } else {
                Log.d("GetBlizzardToken", "Ha habido un error.")
            }
        }
    }
}